import logging
import time
import psutil
from datetime import datetime
from ctypes import (
    windll,
    c_bool,
    c_ulong,
    byref,
    c_int,
    POINTER,
    WINFUNCTYPE,
    create_unicode_buffer
)

from .inputs.keysConstants import KEYS
from .abstract_controller import AbstractController

LOGGER = logging.getLogger(__name__)


class MessageValidateError(Exception):
    """
    Custom Message Validator Exception for your message.
    """

class AdvancedController(AbstractController):
    """
    Advanced Controller uses PostMessageA to execute fake keystrokes directly to the window (hwnd).
    So you may have few game clients (dual-box) and handle events just using pids and hwnds.
    If your gameGuard filters messages under the game's process, you will not find benefits from this class.

    If your game can be started under VM, try basic Controller() where you will send inputs directly to the Windows,
    and active game's window will catch them and use.
    """
    _POST_MESSAGE_A = windll.user32.PostMessageA
    _MOVE_WINDOW = windll.user32.MoveWindow
    _GET_WINDOW_TEXT = windll.user32.GetWindowTextW
    _GET_WINDOW_TEXT_LENGTH = windll.user32.GetWindowTextLengthW
    _IS_WINDOW_VISIBLE = windll.user32.IsWindowVisible
    _GET_WINDOW_THREAD_PROCESS_ID = windll.user32.GetWindowThreadProcessId
    _ENUM_WINDOWS = windll.user32.EnumWindows
    _ENUM_WINDOWS_PROC = WINFUNCTYPE(c_bool, POINTER(c_int), POINTER(c_int))

    WM_KEYDOWN = 0x0100
    WM_KEYUP = 0x0101
    WM_CHAR = 0x0102
    WM_UNICHAR = 0x0109
    WM_SYSCHAR = 0x0106

    __DATA = []
    SEND_KEY_DELAY = 0.1

    DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
    EXPECTED_TITLE = 'Lineage II'

    def __init__(self, hwnd=None, expected_title=None):
        if expected_title:
            self.EXPECTED_TITLE = expected_title

        if not hwnd:
            data = self.get_hwnd()
            if len(data) == 1:
                self.hwnd = data[0]['hwnd']
                LOGGER.info("AdvancedController has been built with given hwnd: %s", self.hwnd)
            else:
                raise ValueError("Script tried automatically load hwnd for given title '{}' but there were more "
                                 "results for given one. Script does not pick the hwnd by itself from more results than 1."
                                 "To proper use, find all hwnd by your own using static method get_hwnd() and pick "
                                 "which hwnd should be handled by the AdvancedController.".format(self.EXPECTED_TITLE))
        else:
            LOGGER.debug("Hwnd %s has been set.", hwnd)
            self.hwnd = hwnd

    def press_key(self, hex_key_code):
        pass

    def release_key(self, hex_key_code):
        pass

    def send_key(self, hex_key_code, delay=20):
        pass

    def send(self, message):
        """
        Before send message, it will be validated.

        :param message: String, message.
        :return: None
        """
        try:
            words = self._validate_message(message)
            self.post_message_a(self.hwnd, KEYS['ENTER'])
            self.post_message_a(self.hwnd, KEYS['BACKSPACE'])
            for word in words:
                for char in word:
                    self.post_message_a(self.hwnd, KEYS[char])
                self.post_message_a(self.hwnd, KEYS['SPACE'])
            time.sleep(self.SEND_KEY_DELAY)
            self.post_message_a(self.hwnd, KEYS['ENTER'])
        except MessageValidateError as ex:
            LOGGER.error("Your message did not passed validation. %s.", ex)

    @classmethod
    def get_hwnd(cls, process_title=None):
        """
        Get all possible window handlers which has given title.

        :param process_title: title for search. Default is settable from class configuration.
        :return: list of dictionaries.
        """
        if process_title:
            cls.EXPECTED_TITLE = process_title
        else:
            LOGGER.debug("You did not specify process_title. Optional will be taken: '%s'", cls.EXPECTED_TITLE)

        cls._ENUM_WINDOWS(cls._ENUM_WINDOWS_PROC(cls.__search_window), 0)
        return cls.__DATA

    @classmethod
    def post_message_a(cls, hwnd, char):
        """
        Send to the hwnd given character. It most games, with GameGuards it may not work with all kind of signs.
        For example sign "/" in few games represent command to execute. Also hotkeys F1-F12 may be a skills.
        All those possibilities may be protected, as the postMessage() does not "simulate" keyboard, but send afaik keystroke.
        It can be easily filtered by gameGuards.

        :param hwnd: window handler.
        :param char: hex character, for example: 0x01
        :return: None
        """
        cls._POST_MESSAGE_A(hwnd, cls.WM_KEYDOWN, char, 0)
        cls._POST_MESSAGE_A(hwnd, cls.WM_KEYUP, 0, 0)

    @classmethod
    def _validate_message(cls, message):
        """
        To validate message, we need to check if we have defined in KEYS dictionary all characters.
        First, method set lower case, remove whitespaces with strip and split message, to remove spaces between words.
        Then build list of characters (we need join all words again without spaces).

        :param message: string message.
        :return: list of words if success, otherwise empty list.
        """
        words = message.lower().strip().split()
        chars = list(''.join(words))
        for char in chars:
            if char not in KEYS:
                raise MessageValidateError(
                    "ERROR: char: '{}' and his hex_code is not defined in your KEYS dictionary. Add it.".format(char))
        return words

    @classmethod
    def __search_window(cls, hwnd, lParam):
        """
        Do not use this method standalone. It's implemented for get_hwnd() method.
        It's a callback, for a windll.user32.EnumWindows(callback, lParam), which scans processes.
        During iteration, we can find hwnd.

        :param hwnd: not settable. Leave it.
        :param lParam: optional lParam.
        :return: True
        """
        length = cls._GET_WINDOW_TEXT_LENGTH(hwnd)
        buffer = create_unicode_buffer(length + 1)
        cls._GET_WINDOW_TEXT(hwnd, buffer, length + 1)
        if cls.EXPECTED_TITLE.lower() in buffer.value.lower():
            c_pid = c_ulong()
            hwnd_pid = cls._GET_WINDOW_THREAD_PROCESS_ID(hwnd, byref(c_pid))
            game_pid = c_pid.value
            o_pid = psutil.Process(game_pid)
            created_time = datetime.fromtimestamp(o_pid.create_time()).strftime(cls.DATE_FORMAT) or None
            data = {
                'title': buffer.value,
                'pid': game_pid,
                'hwnd': hwnd,
                'hwnd_pid': hwnd_pid,
                'time': created_time
            }
            LOGGER.debug("Found: %s", data)
            if not any(existing_data['pid'] == game_pid for existing_data in cls.__DATA):
                cls.__DATA.append(data)
        return True
