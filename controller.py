import ctypes
import time
from ctypes import wintypes

from .inputs import keyboard, hardware, mouse, keysConstants
from .inputs.keysConstants import KEYS
from .abstract_controller import AbstractController
import logging

LOGGER = logging.getLogger(__name__)

wintypes.ULONG_PTR = wintypes.WPARAM

class MessageValidateError(Exception):
    """
    Custom Message Validator Exception for your message.
    """

class INPUT(ctypes.Structure):
    class _INPUT(ctypes.Union):
        _fields_ = [
            ("ki", keyboard.KEYBDINPUT),
            ("mi", mouse.MOUSEINPUT),
            ("hi", hardware.HARDWAREINPUT)
        ]
    _anonymous_ = ["_input"]
    _fields_ = [
        ("type",   wintypes.DWORD),
        ("_input", _INPUT)
    ]


class Controller(AbstractController):
    """
    Send keystroke directly to the Windows.
    Game can be affected if the window is active.
    """
    USER32 = ctypes.WinDLL('user32', use_last_error=True)

    def __init__(self):
        self.LPINPUT = ctypes.POINTER(INPUT)
        self.USER32.SendInput.errcheck = self._check_count
        self.USER32.SendInput.argtypes = [
            wintypes.UINT,  # nInputs
            self.LPINPUT,  # pInputs
            ctypes.c_int  # cbSize
        ]

    def press_key(self, hex_key_code):
        x = INPUT(type=keyboard.INPUT_KEYBOARD,
                  ki=keyboard.KEYBDINPUT(wVk=hex_key_code))
        self.USER32.SendInput(1, ctypes.byref(x), ctypes.sizeof(x))

    def release_key(self, hex_key_code):
        x = INPUT(type=keyboard.INPUT_KEYBOARD,
                  ki=keyboard.KEYBDINPUT(wVk=hex_key_code,
                                         dwFlags=keyboard.KEYEVENTF_KEYUP))
        self.USER32.SendInput(1, ctypes.byref(x), ctypes.sizeof(x))

    def send_key(self, hex_key_code, delay=20):
        self.press_key(hex_key_code)
        time.sleep(delay/1000)
        self.release_key(hex_key_code)

    def send(self, message):
        """
        Before send message, it will be validated.

        :param message: String, message.
        :return: None
        """
        try:
            words = self._validate_message(message)
            self.send_key(keysConstants.KEYS['ENTER'])
            self.send_key(keysConstants.KEYS['BACKSPACE'])
            for word in words:
                for char in word:
                    self.send_key(keysConstants.KEYS[char])
                self.send_key(keysConstants.KEYS['SPACE'])
            self.send_key(keysConstants.KEYS['ENTER'])
        except MessageValidateError as ex:
            LOGGER.error("Your message did not passed validation. %s.", ex)
        return True

    def _check_count(self, result, func, args):
        if result == 0:
            raise ctypes.WinError(ctypes.get_last_error())
        return args

    def _validate_message(self, message):
        """
        To validate message, we need to check if we have defined in KEYS dictionary all characters.
        First, method set lower case, remove whitespaces with strip and split message, to remove spaces between words.
        Then build list of characters (we need join all words again without spaces).

        :param message: string message.
        :return: list of words if success, otherwise empty list.
        """
        words = message.lower().strip().split()
        chars = list(''.join(words))
        for char in chars:
            if char not in KEYS:
                raise MessageValidateError(
                    "ERROR: char: '{}' and his hex_code is not defined in your KEYS dictionary. Add it.".format(char))
        return words
