from abc import ABC, abstractmethod


class AbstractController(ABC):

    @abstractmethod
    def press_key(self, hex_key_code):
        pass

    @abstractmethod
    def release_key(self, hex_key_code):
        pass

    @abstractmethod
    def send_key(self, hex_key_code, delay=20):
        pass

    @abstractmethod
    def send(self, message):
        """
        Before send message, it will be validated.

        :param message: String, message.
        :return: None
        """

    @abstractmethod
    def _validate_message(self, message):
        """
        To validate message, we need to check if we have defined in KEYS dictionary all characters.
        First, method set lower case, remove whitespaces with strip and split message, to remove spaces between words.
        Then build list of characters (we need join all words again without spaces).

        :param message: string message.
        :return: list of words if success, otherwise empty list.
        """
